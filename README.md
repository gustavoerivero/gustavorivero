﻿# Tarea de Git -> Laboratorio II

_Esta tarea corresponde a la asignación de práctica en sistema de versiones Git para la asignatura Laboratorio II del 
Programa de Ingeniería en Informática del Decanato de Ciencias y tecnología de la Universidad 
Centroccidental "Lisandro Alvarado", Venezuela._

## Comenzando 🚀

_Para ello, se creo el presente repositorio, cuyo nombre corresponde al nombre y apellido del estudiante sin espacios (GustavoRivero)._


### Pre-requisitos 📋

* Tener instalado [Git](https://git-scm.com/)

### Clonar Repositorio 🔧

_Para comenzar, se procede clonando el repositorio en cuestión por medio del comando en consola:_

```
git clone https://gustavoerivero@bitbucket.org/gustavoerivero/gustavorivero.git
```

_Luego, situarse en la carpeta clonada del repositorio "gustavorivero"._

_Allí se podrá observar los archivos subidos a este repositorio._

## Verificar commits 📦

_Para verificar los commits, redirigirse al repositorio por medio del enlace:_

```
https://bitbucket.org/gustavoerivero/gustavorivero/commits/
```

_Allí se podrá apreciar el historial de commits realizados en este repositorio._

## Autor ✒️

_El autor de esta práctica es:_

* **Gustavo Rivero** - [gustavoerivero](https://github.com/gustavoerivero)



---
⌨️ con ❤️ por [Gustavo Rivero](https://github.com/gustavoerivero)
